/* Чистый JS */
/* Сокращатель ссылок*/
var form = document.querySelector('#link-form');

form.addEventListener('submit', function(evt) {
evt.preventDefault();

/* Получаем данные из полей формы */
var formData = {
    link: document.querySelector('input[name="link"]').value,
};

var request = new XMLHttpRequest();
request.addEventListener('load', function() {
    console.log(request.response);
    var short_link = request.response;
    form.innerHTML = "Конечный результат:" + short_link;
});

/* Отправляем запрос без перезагрузки страницы */
request.open('POST', 'functions.php', true);
request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
request.send('link=' + encodeURIComponent(formData.link));
});


// Переключение табов
  
  $(document).ready(function(){
      $('.tabs-wrapper li').click(function() {
          $(this).siblings('li').removeClass('active');
          $(this).addClass('active');
      });
  });
  
  $('.description_tab').click(function() {
    $('#tab-description').css("display", "block");
    $('#tab-reviews, #tab-about-brands').css("display", "none");
  });
  
  $('.reviews_tab').click(function() {
    $('#tab-description, #tab-about-brands').css("display", "none");
    $('#tab-reviews').css("display", "block");
  });
  
  $('.about-brands').click(function() {
    $('#tab-description, #tab-reviews').css("display", "none");
    $('#tab-about-brands').css("display", "block");
  });

//Изменение визуального отображения каталога
  
  (function($){
    "use strict";
    var lastScrollTop=0;
    $(window).on('scroll',function(){
      var scrollTop=$(this).scrollTop();
      if(scrollTop>lastScrollTop){
        $('#mobile-navbar.hide_on_footer').removeClass('active')
      }else{
        $('#mobile-navbar.hide_on_footer').addClass('active')
      }
      lastScrollTop=scrollTop
    })
  })(jQuery);
  
  $('.bi.bi-grid-3x3-gap-fill').addClass('active');
  $('.bi.bi-grid-3x3-gap-fill').on('click',function(){
    $(this).addClass('active');
    $('.bi.bi-list-ul').removeClass('active');
    Cookies.set('flexcookie','flex',{path:''});
    $('ul.products').fadeOut(300,function(){
      $(this).addClass('flex').removeClass('list').fadeIn(300)
    });
      return!1
    });
    $('.bi.bi-list-ul').on('click',function(){
      $(this).addClass('active');
      $('.bi.bi-grid-3x3-gap-fill').removeClass('active');
      Cookies.set('flexcookie','list',{path:''});
      $('ul.products').fadeOut(300,function(){
        $(this).addClass('list').removeClass('flex').fadeIn(300)
      });return!1});
      if(Cookies.get('flexcookie')=='flex'){
        $('.bi.bi-grid-3x3-gap-fill').addClass('active');
        $('.bi.bi-list-ul').removeClass('active');
        $('ul.products').addClass('flex').removeClass('list')
      }
      if(Cookies.get('flexcookie')=='list'){
        $('.bi.bi-list-ul').addClass('active');
        $('.bi.bi-grid-3x3-gap-fill').removeClass('active');
        $('ul.products').addClass('list').removeClass('flex')
      }else{
        Cookies.remove('flexcookie',{path:''})
      }
  
/* Скрипт для аккордеона блока FAQ */
jQuery(document).ready(function(){
  jQuery('.faq-block .faq__title').on('click', accordeon_faq);
});

function accordeon_faq(){
  jQuery('.faq-block .faq__content').not(jQuery(this).next()).slideUp(500);
  jQuery(this).next().slideToggle(500);
}
